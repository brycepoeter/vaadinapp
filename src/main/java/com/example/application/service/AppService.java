package com.example.application.service;

import com.example.application.models.AppsList;
import com.example.application.models.Result;
import com.example.application.models.AppsList;
import com.example.application.repository.AppRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;


import java.util.List;


@Service
public class AppService {

    public static final int MAX_RESULTS = 20;
    private AppRepository appRepository;

    public AppService(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    //READ paged
    public void getApps(AsyncRestCallback<List<Result>> callback, String search, int maxResults,
                         int startIndex) {

        System.out.println("Search string" + search);

        System.out.println("fetching apps -> " + startIndex + " to "
                + (startIndex + MAX_RESULTS - 1));

        appRepository.getApps(callback, search, maxResults, startIndex);


    }

}