package com.example.application.repository;

import com.example.application.models.Result;
import com.example.application.models.AppsList;
import com.example.application.service.AsyncRestCallback;
import com.example.application.service.AppService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Repository
public class AppRepository {



    //READ paged
    public void getApps(AsyncRestCallback<List<Result>> callback, String search, int maxResults,
                        int startIndex) {


        String raw = "https://itunes.apple.com/search?term=%s+app&country=us&entity=software&limit=%d&offset=%d";
        String formatted = String.format(raw, search, maxResults, startIndex);

        System.out.println("Before WebClient");
        WebClient.RequestHeadersSpec<?> spec = WebClient.create().get().uri(formatted);
        System.out.println("After WebClient");

        //spec.retrieve().toEntity(AppsList.class).subscribe(result -> {
        spec.accept(MediaType.APPLICATION_JSON).retrieve().toEntity(AppsList.class).subscribe(result -> {
            System.out.println("Before casting");
            System.out.println(result);


            final AppsList appsList = result.getBody();
            //appsList.setResults(result);

            if (null == AppsList.getResults()) {
                System.out.println("Apps list null");
                return;
            }

            callback.operationFinished(appsList.getResults());

        });

    }
}
