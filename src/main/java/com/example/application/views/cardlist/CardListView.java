package com.example.application.views.cardlist;

import java.util.ArrayList;
import java.util.List;

import com.example.application.models.AppsList;
import com.example.application.models.Result;
import com.example.application.service.AppService;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.KeyDownEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.PageTitle;
import com.example.application.views.main.MainView;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.component.dependency.CssImport;
import elemental.json.JsonObject;


@Route(value = "card-list", layout = MainView.class)
@RouteAlias(value = "", layout = MainView.class)
@PageTitle("Card List")
@CssImport("./views/cardlist/card-list-view.css")
public class CardListView extends Div implements AfterNavigationObserver {


    private AppService appService;
    private Grid<Result> grid = new Grid<>();
    private int startIndex = 0;
    private boolean isLoading = false;

    private List<Result> results = new ArrayList<>();
    private TextField textField;
    private String searchTerm;

    public CardListView(AppService appService) {
        this.appService = appService;
        textField = new TextField();
        textField.setLabel("Search Term");
        textField.setPlaceholder("search...");
        textField.setAutofocus(true);
        textField.setWidthFull();
        textField.addKeyDownListener(keyDownEvent -> {
                    String keyStroke = keyDownEvent.getKey().getKeys().toString();
                    if (keyStroke.equals("[Enter]") && !isLoading) {
                        System.out.println(textField.getValue());
                        searchTerm = textField.getValue();
                        startIndex = 0;
                        results.clear();
                        getApps(searchTerm);
                    }
                }
        );
        addClassName("card-list-view");
        setSizeFull();
        grid.setHeight("100%");
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_NO_ROW_BORDERS);
        grid.addComponentColumn(result -> createCard(result));
        grid.addItemClickListener(new ComponentEventListener<ItemClickEvent<Result>>() {
            @Override
            public void onComponentEvent(ItemClickEvent<Result> itemItemClickEvent) {
                System.out.println(itemItemClickEvent.getItem());
            }
        });

        add(textField, withClientsideScrollListener(grid));
    }

    private void getApps(String searchTerm) {
        isLoading = true;

        appService.getApps(appsList -> {
            //we must fire this callback on the UI thread and that is why we use getUi().get().acccess(() ->
            getUI().get().access(() -> {

                //this is the callback result, so volumesResponse is the volumesResponse returned from
                //      void operationFinished(T results);
                results.addAll(appsList);
                System.out.println("vvvvvvv grid");
                grid.setItems(results);
                System.out.println(grid);
                System.out.println("^^^^^ grid");
                startIndex += AppService.MAX_RESULTS;
                isLoading = false;
                getUI().get().push();

            });
        }, searchTerm, AppService.MAX_RESULTS, startIndex);
    }


    private Grid<Result> withClientsideScrollListener(Grid<Result> grid) {
        grid.getElement().executeJs(
                "this.$.scroller.addEventListener('scroll', (scrollEvent) => " +
                        "{requestAnimationFrame(" +
                        "() => $0.$server.onGridScroll({sh: this.$.table.scrollHeight, " +
                        "ch: this.$.table.clientHeight, " +
                        //"Content-Type: application/json" +
                        "st: this.$.table.scrollTop}))},true)",
                getElement());
        return grid;
    }

    @ClientCallable
    public void onGridScroll(JsonObject scrollEvent) {
        int scrollHeight = (int) scrollEvent.getNumber("sh");
        int clientHeight = (int) scrollEvent.getNumber("ch");
        int scrollTop = (int) scrollEvent.getNumber("st");

        double percentage = (double) scrollTop / (scrollHeight - clientHeight);
        //reached the absolute bottom of the scroll
        if (percentage == 1.0) {

            if (!isLoading) {
                getApps(searchTerm);
            }
            grid.scrollToIndex(results.size() / 2);

        }

    }

    private HorizontalLayout createCard(Result result) {
        HorizontalLayout card = new HorizontalLayout();
        card.addClassName("card");
        card.setSpacing(false);
        card.getThemeList().add("spacing-s");

        Image image = new Image();
        image.setSrc(getSmallThumbnail(result));
        VerticalLayout description = new VerticalLayout();
        description.addClassName("description");
        description.setSpacing(false);
        description.setPadding(false);

        HorizontalLayout header = new HorizontalLayout();
        header.addClassName("header");
        header.setSpacing(false);
        header.getThemeList().add("spacing-s");

        Span name = new Span(getAuthor(result));
        name.addClassName("name");
        Span date = new Span(getPublisher(result));
        date.addClassName("date");
        header.add(name, date);


        description.add(header);
        card.add(image, description);
        return card;
    }

    private String getPublisher(Result result) {
        if (null == result || null == result.getSellerName()) {
            return "";
        }
        return result.getSellerName();
    }

    private String getAuthor(Result result) {
        if (null == result || null == result.getSellerUrl()) {
            return "";
        }
        return result.getSellerUrl();
    }

    private String getSmallThumbnail(Result result) {
        if (null == result || null == result.getScreenshotUrls()) {
            return "https://randomuser.me/api/portraits/men/16.jpg";
        }
        return result.getScreenshotUrls().get(0);
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        // we don't need to do anything here now. Possibly remove interface and this contract-method.
    }


}